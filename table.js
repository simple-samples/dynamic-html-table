var table = document.getElementById("shapes-edges");
var index = 1
for (shape in shapes) {
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(-1);
    cell1.innerHTML = Object.keys(shapes)[index];
    var cell2 = row.insertCell(-1);
    cell2.innerHTML = shapes[shape];
    index = index + 1
}